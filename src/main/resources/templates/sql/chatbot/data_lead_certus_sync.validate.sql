select 
state_id,
DATEDIFF(STR_TO_DATE(:update_datetime,'%Y-%m-%d'),date(update_datetime)) as diffDay,
TIMESTAMPDIFF(SECOND,STR_TO_DATE(:update_datetime,'%Y-%m-%d %T'),update_datetime) as diffSeconds
from data_lead_certus_sync
where    telephone =:telephone
order by update_datetime desc 
limit 1