insert into data_lead_certus_sync (
client_id,customer_id,lead_id, state_id,telephone , insert_datetime,update_datetime)
values 
(:client_id,:customer_id,:lead_id, :state_id,:telephone,
STR_TO_DATE(:insert_datetime,'%Y-%m-%d %T'),STR_TO_DATE(IFNULL(:update_datetime,:insert_datetime),'%Y-%m-%d %T')
)