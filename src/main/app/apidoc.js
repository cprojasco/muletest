/**
@api {post} /lead/create  [POST - crear lead en recruit] 
@apiName LeadCreate
@apiGroup Lead
@apiDescription Crea lead en recruit y lanza accion requerida

@apiHeader {String} Content-Type tipo de contenido a enviar  
@apiHeader {String} X-token token de valiacion

@apiHeaderExample header
                   Content-Type  : application/json
                   X-token : 123456 

@apiParam {String} telefono  Telefono del lead
@apiParam {String} correo  correo del lead
@apiParam {String} nombres  nombres del lead
@apiParam {String} apellidoPaterno  apellido Paterno del lead
@apiParam {String} apellidoMaterno  apellido Materno del lead
@apiParam {String} fuenteOrigen  fuenteOrigen del lead  (usar los parametros del ejemplo, van a variar)
@apiParam {String} detalleOrigen  detalleOrigen del lead (usar los parametros del ejemplo, van a variar)
@apiParam {String} medioDifusion  medioDifusion del lead (usar los parametros del ejemplo, van a variar)
@apiParam {String} numeroDocumentoIdentidad  numero Documento de Identidad del lead 
@apiParam {String} sede  sedeId del lead 
@apiParam {String} esAlumnoCertus  indica si es alumno certus, enviará correo a academicos y no registrara en recruit
@apiParam {String} esAlumnoOtros  indica si es alumno de otra institución , no realizara ninguna acción. 
@apiParam {String} accionRequerida  acción que debe realizar una vez credo el lead "llamada" o "ninguno"
 
@apiParamExample {json} Request-Example:
{   "telefono": "+51 969744885",
	"correo": "isaac1.meji122a@gmail.com",
	"nombres": "edilberto",
    "apellidoPaterno": "yyyyyyy",
    "apellidoMaterno": "Flores",
    "fuenteOrigen":  "D4B6A6EB-C7FA-E711-80D5-0A0E55030E2C",
    "detalleOrigen":       "59C1CFCD-C7FA-E711-80D5-0A0E55030E2C",
    "medioDifusion" : "AE5DA2B7-C7FA-E711-80D0-0EA83F1D7C1A",
    "numeroDocumentoIdentidad":"41951111",
    "sede":"AQP",
    "esAlumnoCertus" :"no",
    "esAlumnoOtros" :"no",
    "accionRequerida":"llamada"
}


@apiSuccess {String} status estado de conexion

@apiSuccessExample {json} Success-Response:
                {"status": "ok"}
*/
